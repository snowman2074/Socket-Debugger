using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace SocketClient
{

    /// <summary>
    /// 
    /// </summary>
    public class LogManager
    {
        /// <summary>
        /// 
        /// </summary>
        public List<LogItem> Items
        {
            get
            {
                if (_items == null)
                {
                    _items = new List<LogItem>();
                }
                return _items;
            }
            set { _items = value; }
        } private List<LogItem> _items;
    }
}
