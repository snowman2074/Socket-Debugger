﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;

namespace SocketClient
{
    /// <summary>
    /// 
    /// </summary>
    public class KeyPressHelper
    {
        internal static void Process(object sender, System.Windows.Forms.KeyPressEventArgs e)
        {
            if ((Control.ModifierKeys & Keys.Control) == 0)
            {
                if (!IsAllowedKey(e.KeyChar))
                {
                    e.KeyChar = (char)Keys.None;
                }
            }
        }

        private static bool IsAllowedKey(char key)
        {
            return char.IsDigit(key) ||
                                (key >= 'a' && key <= 'f') ||
                                (key >= 'A' && key <= 'F') ||
                                key == (char)System.Windows.Forms.Keys.Back ||
                                key == ' ';
        }
    }
}
