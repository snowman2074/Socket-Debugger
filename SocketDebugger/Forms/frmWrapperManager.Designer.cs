﻿namespace SocketClient
{
    partial class frmWrapperManager
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lvWrapper = new System.Windows.Forms.ListView();
            this.chName = new System.Windows.Forms.ColumnHeader();
            this.chDescription = new System.Windows.Forms.ColumnHeader();
            this.btnOK = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lvWrapper
            // 
            this.lvWrapper.CheckBoxes = true;
            this.lvWrapper.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.chName,
            this.chDescription});
            this.lvWrapper.FullRowSelect = true;
            this.lvWrapper.GridLines = true;
            this.lvWrapper.Location = new System.Drawing.Point(12, 12);
            this.lvWrapper.MultiSelect = false;
            this.lvWrapper.Name = "lvWrapper";
            this.lvWrapper.Size = new System.Drawing.Size(589, 151);
            this.lvWrapper.TabIndex = 0;
            this.lvWrapper.UseCompatibleStateImageBehavior = false;
            this.lvWrapper.View = System.Windows.Forms.View.Details;
            this.lvWrapper.ItemChecked += new System.Windows.Forms.ItemCheckedEventHandler(this.lvWrapper_ItemChecked);
            // 
            // chName
            // 
            this.chName.Text = "名称";
            this.chName.Width = 182;
            // 
            // chDescription
            // 
            this.chDescription.Text = "描述";
            this.chDescription.Width = 396;
            // 
            // btnOK
            // 
            this.btnOK.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnOK.Location = new System.Drawing.Point(526, 169);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(75, 23);
            this.btnOK.TabIndex = 1;
            this.btnOK.Text = "关闭";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // frmWrapperManager
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btnOK;
            this.ClientSize = new System.Drawing.Size(613, 203);
            this.Controls.Add(this.btnOK);
            this.Controls.Add(this.lvWrapper);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmWrapperManager";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "包装";
            this.Load += new System.EventHandler(this.frmWrapperManager_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ListView lvWrapper;
        private System.Windows.Forms.ColumnHeader chName;
        private System.Windows.Forms.ColumnHeader chDescription;
        private System.Windows.Forms.Button btnOK;
    }
}