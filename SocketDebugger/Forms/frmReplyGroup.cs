﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SocketClient
{
    public partial class frmReplyGroup : Form
    {
        ReplyGroup _replyGroup = null;
        public frmReplyGroup(ReplyGroup replyGroup)
        {
            InitializeComponent();

            _replyGroup = replyGroup;

            this.textBox1.Text = _replyGroup.Name;
        }

        private void frmReplyGroup_Load(object sender, EventArgs e)
        {

        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            _replyGroup.Name = this.textBox1.Text.Trim();
            this.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.Close();
        }
    }
}
