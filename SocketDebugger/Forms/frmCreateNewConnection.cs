﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SocketClient
{
    public partial class frmCreateNewConnection : Form
    {
        public frmCreateNewConnection()
        {
            InitializeComponent();
            this.rbTcp.Tag = System.Net.Sockets.ProtocolType.Tcp;
            this.rbUdp.Tag = System.Net.Sockets.ProtocolType.Udp;
        }

        #region Config
        private Config Config
        {
            get { return App.Default.Config; }
        }
        #endregion //Config

        private void frmCreateNewConnection_Load(object sender, EventArgs e)
        {
            Config.PortList.Sort();
            this.cmbPort.DataSource = Config.PortList;
            this.cmbPort.Text = Config.LastPort.ToString();

            Config.IPAddressList.Sort();
            this.cmbIPAddress.DataSource = Config.IPAddressList;
            this.cmbIPAddress.Text = Config.LastIPAddress;

            rbTcp.Checked = Config.LastProtocolType.Equals(rbTcp.Tag);
            rbUdp.Checked = Config.LastProtocolType.Equals(rbUdp.Tag);
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnConnect_Click(object sender, EventArgs e)
        {
            if (VerifyIPAddressAndPort() && Connnect())
            {
                this.DialogResult = System.Windows.Forms.DialogResult.OK;
                this.Close();
            }
        }

        #region VerifyIPAddressAndPort
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        private bool VerifyIPAddressAndPort()
        {
            IPAddress ipaddress = null;
            UInt16 port = 0;

            bool b = false;
            b = IPAddress.TryParse(this.cmbIPAddress.Text, out ipaddress);
            if (!b)
            {
                NUnit.UiKit.UserMessage.DisplayFailure(Strings.InvalidIPAddress);
                return false;
            }
            b = UInt16.TryParse(this.cmbPort.Text, out port);
            if (!b)
            {
                NUnit.UiKit.UserMessage.DisplayFailure(Strings.InvalidPort);
                return false;
            }

            return true;
        }
        #endregion //VerifyIPAddressAndPort

        private ProtocolType GetConnectProtocolType()
        {
            if (rbTcp.Checked)
            {
                return ProtocolType.Tcp;
            }
            if (rbUdp.Checked)
            {
                return ProtocolType.Udp;
            }
            throw new NotSupportedException();
        }

        #region Connnect
        /// <summary>
        /// 
        /// </summary>
        private bool Connnect()
        {
            if (!VerifyIPAddressAndPort())
                return false;

            this.Config.MarkIPAddress(this.IPAddress);
            this.Config.MarkPort(this.Port);
            this.Config.MarkProtocolType(this.GetConnectProtocolType());

            try
            {
                if (Config.IsUseLocalPort)
                {
                    this.SocketClient.Connect(this.IPAddress, this.Port, Config.LocalPort, GetConnectProtocolType());
                }
                else
                {
                    this.SocketClient.Connect(this.IPAddress, this.Port, GetConnectProtocolType());
                }
            }
            catch (SocketException socketEx)
            {
                NUnit.UiKit.UserMessage.DisplayFailure(socketEx.Message);
                return false; ;
            }
            //SetConnectState();
            this.SocketClient.BeginReceive();
            return true;
        }
        #endregion //Connnect

        #region SocketClient
        private SocketClient SocketClient
        {
            get
            {
                return App.Default.SocketClient;
            }
        }
        #endregion //SocketClient

        #region IPAddress
        /// <summary>
        /// 
        /// </summary>
        private IPAddress IPAddress
        {
            get
            {
                return IPAddress.Parse(this.cmbIPAddress.Text);
            }
            set
            {
                this.cmbIPAddress.Text = value.ToString();
            }
        }
        #endregion //IPAddress

        #region Port
        private UInt16 Port
        {
            get
            {
                return UInt16.Parse(this.cmbPort.Text);
            }
            set
            {
                cmbPort.Text = value.ToString();
            }
        }
        #endregion //Port
    }
}
